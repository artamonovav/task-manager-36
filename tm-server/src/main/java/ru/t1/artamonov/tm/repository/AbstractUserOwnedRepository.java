package ru.t1.artamonov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.constant.DmlConstant;
import ru.t1.artamonov.tm.api.repository.IUserOwnedRepository;
import ru.t1.artamonov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel>
        extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final String userId, @Nullable final M model);

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?;", getTableName(), DmlConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?;", getTableName(), DmlConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                while (rowSet.next()) result.add(fetch(rowSet));
                return result;
            }
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? ORDER_BY %s;", getTableName(), DmlConstant.USER_ID, getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                while (rowSet.next()) result.add(fetch(rowSet));
                return result;
            }
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(
            final @Nullable String userId,
            final @Nullable String id
    ) {
        if (userId == null || id == null) return null;
        @NotNull final String sql = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ? LIMIT 1;", getTableName(), DmlConstant.ROW_ID, DmlConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                if (!rowSet.next()) return null;
                return fetch(rowSet);
            }
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        @NotNull final String sql = String.format(
                "SELECT COUNT(1) FROM %s WHERE %s = ?;", getTableName(), DmlConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getLong("count");
            }
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || model == null) return null;
        @NotNull final String sql = String.format(
                "DELETE FROM %s WHERE %s = ? AND %s = ?;", getTableName(), DmlConstant.USER_ID, DmlConstant.ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            statement.setString(1, model.getUserId());
            statement.setString(2, model.getId());
            statement.executeUpdate();
            return model;
        }
    }

    @Nullable
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

}
