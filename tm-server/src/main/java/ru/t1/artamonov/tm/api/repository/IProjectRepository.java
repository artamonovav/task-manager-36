package ru.t1.artamonov.tm.api.repository;

import ru.t1.artamonov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project>, IUserOwnedRepository<Project> {

}
