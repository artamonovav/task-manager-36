-- Table: public.tm_user

-- DROP TABLE IF EXISTS public.tm_user;

CREATE TABLE IF NOT EXISTS public.tm_user
(
    row_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    login character varying(50) COLLATE pg_catalog."default" NOT NULL,
    pswrd_hash character varying(200) COLLATE pg_catalog."default" NOT NULL,
    fst_name character varying(100) COLLATE pg_catalog."default",
    lst_name character varying(100) COLLATE pg_catalog."default",
    mdl_name character varying(100) COLLATE pg_catalog."default",
    email character varying(200) COLLATE pg_catalog."default",
    role character varying(50) COLLATE pg_catalog."default",
    lock_flg boolean NOT NULL,
    CONSTRAINT "TM_USER_pkey" PRIMARY KEY (row_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_user
    OWNER to tm;

COMMENT ON TABLE public.tm_user
    IS 'TASK MANAGER USER TABLE';